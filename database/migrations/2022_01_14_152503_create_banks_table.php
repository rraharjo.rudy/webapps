<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->id();
            $table->string('code', 20);
            $table->string('name', 100);
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('banks')->insert(
            array(
                array('id' => 1, 'name' => 'Bank Central Asia', 'code' => 'BCA'),
                array('id' => 2, 'name' => 'Bank Negara Indonesia', 'code' => 'BNI'),
                array('id' => 3, 'name' => 'Bank Republik Indonesia', 'code' => 'BRI'),
                array('id' => 4, 'name' => 'Bank Mandiri', 'code' => 'Mandiri')
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
