<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_partners', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('business_partner_categories');
            
            $table->unsignedBigInteger('business_partner_title_id')->unsigned()->index();
            $table->foreign('business_partner_title_id')->references('id')->on('business_partner_titles');

            $table->unsignedBigInteger('business_partner_group_id')->unsigned()->index();
            $table->foreign('business_partner_group_id')->references('id')->on('business_partner_groups');

            $table->unsignedBigInteger('business_partner_type_id')->unsigned()->index();
            $table->foreign('business_partner_type_id')->references('id')->on('business_partner_types');

            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->boolean('activated_status')->default(true);
            $table->date('periode_start')->nullable();
            $table->date('periode_end')->nullable();
            $table->unsignedBigInteger('bill_to_address')->nullable();
            $table->unsignedBigInteger('ship_to_address')->nullable();
            $table->unsignedBigInteger('pay_to_address')->nullable();
            $table->longText('noted')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_partners');
    }
}
