<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('code', 50);
            $table->unsignedBigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('branch_id')->unsigned()->index();
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->unsignedBigInteger('identity_card_id')->unsigned()->index();
            $table->foreign('identity_card_id')->references('id')->on('identity_cards');
            $table->unsignedBigInteger('bank_id')->unsigned()->index();
            $table->foreign('bank_id')->references('id')->on('banks');
            $table->unsignedBigInteger('type_id')->after('bank_id')->unsigned()->index();
            $table->foreign('type_id')->references('id')->on('employee_types');
            $table->unsignedBigInteger('job_position_id')->after('type_id')->unsigned()->index();
            $table->foreign('job_position_id')->references('id')->on('employee_job_positions');
            $table->string('nik', 50)->unique();
            $table->string('firstname', 30);
            $table->string('lasname', 30)->nullable();
            $table->string('photo')->nullable();
            $table->string('birth_place', 150)->nullable();
            $table->date('birth_date')->nullable();
            $table->enum('marital_status', ['single', 'married'])->default('single');
            $table->enum('gender', ['male', 'female'])->default('male');
            $table->date('hire_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
