<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateIdentityCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identity_cards', function (Blueprint $table) {
            $table->id();
            $table->string('code', 20);
            $table->string('name', 130);
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('identity_cards')->insert(
            array(
                array('id' => 1, 'name' => 'Kartu Tanda Penduduk', 'code' => 'KTP'),
                array('id' => 2, 'name' => 'Surat Izin Mengemudi', 'code' => 'SIM'),
                array('id' => 3, 'name' => 'Paspor', 'code' => 'Paspor'),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identity_cards');
    }
}
