<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessPartnerPicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_partner_pics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('business_partner_id')->unsigned()->index();
            $table->foreign('business_partner_id')->references('id')->on('business_partners')->onDelete('cascade');
            $table->unsignedBigInteger('business_partner_title_id')->unsigned()->index();
            $table->foreign('business_partner_title_id')->references('id')->on('business_partner_titles');
            $table->string('code');
            $table->string('name')->nullable();
            $table->longText('address')->nullable();
            $table->string('phone_01')->nullable();
            $table->string('phone_02')->nullable();
            $table->string('email')->nullable();
            $table->longText('noted')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_partner_pics');
    }
}
