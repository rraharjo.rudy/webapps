<?php

namespace Database\Seeders;

use App\Imports\CityImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new CityImport, database_path('seeders/csvs/cities.csv'), null, \Maatwebsite\Excel\Excel::CSV);
    }
}
