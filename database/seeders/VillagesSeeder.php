<?php

namespace Database\Seeders;

use App\Imports\VillageImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class VillagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new VillageImport, database_path('seeders/csvs/villages.csv'), null, \Maatwebsite\Excel\Excel::CSV);
    }
}
