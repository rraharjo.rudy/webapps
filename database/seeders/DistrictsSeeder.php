<?php

namespace Database\Seeders;

use App\Imports\DistrictImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class DistrictsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new DistrictImport, database_path('seeders/csvs/districts.csv'), null, \Maatwebsite\Excel\Excel::CSV);
    }
}
