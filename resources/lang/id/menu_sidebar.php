<?php

return [
    'logout' => 'Keluar',
    'managment_user' => 'Manajemen Pengguna',
    'managment_user.team' => 'Departemen',
    'managment_user.user' => 'Pengguna',
    'managment_user.role' => 'Peran',
    'managment_user.permission' => 'Izin Akses',
    'utilities' => "Utilities",
    'utilities_area' => "Area",
    'utilities_province' => "Provinsi",
    'utilities_city' => "Kota / Kabupaten",
    'utilities_district' => "Kecamatan",
    'utilities_village' => "Kelurahan",
];
