<?php

return [
    'logout' => 'Logout',
    'managment_user' => 'User Management',
    'managment_user.team' => 'Department',
    'managment_user.user' => 'Users',
    'managment_user.role' => 'Roles',
    'managment_user.permission' => 'Permissions',
    'utilities' => "Utilities",
    'utilities_area' => "Area",
    'utilities_province' => "Provinces",
    'utilities_city' => "Cities",
    'utilities_district' => "Districts",
    'utilities_village' => "Villages",
];
